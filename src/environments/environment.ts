// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAhUgMMQ29i2jFb3PXDAJssC2apCMDzY6k",
    authDomain: "student-registration-f0bab.firebaseapp.com",
    databaseURL: "https://student-registration-f0bab.firebaseio.com",
    projectId: "student-registration-f0bab",
    storageBucket: "student-registration-f0bab.appspot.com",
    messagingSenderId: "210143665159",
    appId: "1:210143665159:web:c201e7c27f94c3b0ba1ea2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
