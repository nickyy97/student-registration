export class Student {
    id:string;
    firstName:string;
    lastName:string;
    hometown:string;
    mobile:string;
}
