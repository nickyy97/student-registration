import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/shared/student.service';
import { NgForm } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastrService } from 'ngx-toastr';
import { from } from 'rxjs';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(private stuService:StudentService,private firestore:AngularFirestore,private toastr:ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }
  
  resetForm(form?:NgForm){
    if(form!=null)
      form.resetForm();
    this.stuService.formData = {
      id:null,
      firstName:'',
      lastName:'',
      hometown:'',
      mobile:''
    }
  }

  onSubmit(form:NgForm){
    let data =Object.assign({},form.value) ;
    delete data.id;
    if(form.value.id==null){
      this.firestore.collection('student').add(data);
    }
    else
    this.firestore.doc('student/'+form.value.id).update(data);
    this.resetForm(form);
    this.toastr.success('Submitted Successfully!','Student Registration');

  }

}
