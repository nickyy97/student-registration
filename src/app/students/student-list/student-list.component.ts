import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/shared/student.service';
import { Student } from 'src/app/shared/student.model';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  list: Student[];

  constructor(private stuService:StudentService) { }

  ngOnInit() {

    this.stuService.getStudent().subscribe(actionArray=>{
      this.list = actionArray.map(item =>{
        return {
          id :item.payload.doc.id,
          ...item.payload.doc.data()
        } as Student
      })
    });

  }

  onEdit(stu:Student){
    this.stuService.formData =Object.assign({},stu);
  }


}
